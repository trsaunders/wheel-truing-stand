function TSlotPoints(eps=0.2) {
  return [
    [0+eps, -(3-eps)],
    [-(1.5+eps), -(3-eps)],
    [-(1.5+eps), -(6-eps)],
    [-3, -(6-eps)],
    [-(5.7-eps), -3],
    [-(5.7-eps), 3],
    [-3, (6-eps)],
    [-(1.5+eps), (6-eps)],
    [-(1.5+eps), (3-eps)],
    [0+eps, 3-eps],
  ]
}

function profilePoints(eps=0.2) {
  var points = TSlotPoints(eps).concat([[eps, 10+eps]]);

  var allPoints = new Array();

  var transforms = [[1, 0], [0, 1], [-1, 0], [0, -1]];

  for(var i=0; i < 4; i++) {
    for(var j=0; j < points.length; j++) {
      allPoints.push([
        (points[j][0] + 10) * transforms[i][0] - points[j][1] * transforms[i][1],
        (points[j][0] + 10) * transforms[i][1] + points[j][1] * transforms[i][0]
      ]);
    }
  }

  return allPoints;
}

function TSlot(eps=0.2) {
  return CAG.fromPoints(TSlotPoints()).translate([10, 0, 0]);
}

function profile(eps=0.2) {
  var o = new Array();
  for(var i=0; i < 4; i++) {
    o.push(TSlot(eps).rotateZ(90*i));
  }

  return CAG.roundedRectangle({
    center: [0, 0],
    radius: [10+eps, 10+eps],
    roundradius: 1,
    resolution: 50,
  }).subtract(o);
}

chamferedProfile = function(height=10, eps=0.1, chamferEps=0.6) {
  var normalCrossSection = profilePoints(eps);
  var bottomCrossSection = profilePoints(chamferEps);

  var chamferPolygons = new Array();

  var triangles = [];

  var p1 = [];
  var p2 = [];

  var cornerTriangles = [
    [13, 12, 11],
    [13, 11, 10],
    [13, 10, 14],
    [14, 10, 6],
    [6, 10, 7],
    [7, 10, 9],
    [7, 9, 8],
    [14, 6, 5],
    [15, 14, 5],
    [15, 5, 4],
  ];

  var N = normalCrossSection.length;

  var T = cornerTriangles.length;
  for(var i=0; i < 4; i++) {
    for(var j=0; j < T; j++) {
      triangles.push([
        ((cornerTriangles[j][0] + i*11) % N) + N,
        ((cornerTriangles[j][1] + i*11) % N) + N,
        ((cornerTriangles[j][2] + i*11) % N) + N,
      ]);

      triangles.push([
        ((cornerTriangles[j][2] + i*11) % N),
        ((cornerTriangles[j][1] + i*11) % N),
        ((cornerTriangles[j][0] + i*11) % N),
      ]);
    }
  }

  var centreTriangles = [
    [37, 4, 15],
    [37, 15, 26],
    [15 + N, 4 + N, 37 + N],
    [26 + N, 15 + N, 37 + N],
  ];
  
  for(var i=0; i < N; i++) {
    p1.push([bottomCrossSection[i][0], bottomCrossSection[i][1], 0]);
    p2.push([normalCrossSection[i][0], normalCrossSection[i][1], 1]);
    triangles.push([(i%N) + N, (i + 1) % N, (i % N)]);
    triangles.push([(i%N) + N, ((i + 1)%N) + normalCrossSection.length, (i + 1)%N]);
  }

  return polyhedron({
    points: p1.concat(p2),
    triangles: triangles.concat(centreTriangles),
  }).union(
    CAG.fromPoints(
      normalCrossSection
    ).extrude({
      offset: [0, 0, height - 1],
    }).translate([0, 0, 1])
  );
}
