include("TSlot.jscad");
include("params.jscad");

var P = new Params();

function axleJaw() {
  var jaw = hull(
    CAG.circle({
      center: [
        P.jawAxleHeight(),
        P.jawBodyRadius() + P.axleJaw.outerRadius,
      ],
      radius: P.axleJaw.outerRadius,
    }),
    CAG.circle({
      center: [0, 0],
      radius: sqrt((P.jawBodyRadius()*P.jawBodyRadius()) / 2),
    })
  ).subtract(
    hull(
      CAG.circle({
        center: [
          P.jawAxleHeight(),
          P.jawBodyRadius() + P.axleJaw.outerRadius,
        ],
        radius: P.axleJaw.innerRadius,
      }),
      CAG.circle({
        center: [
          P.jawAxleHeight()*1.5,
          (P.jawBodyRadius() + P.axleJaw.outerRadius)*1.5,
        ],
        radius: P.axleJaw.innerRadius*1.5,
      })
    )
  ).extrude({
    offset:[0, 0, P.axleJaw.thickness]
  }).subtract(
    CSG.cylinder({
      start: [P.jawAxleHeight(), P.jawBodyRadius() + P.axleJaw.outerRadius, P.axleJaw.clampThickness],
      end: [P.jawAxleHeight(), P.jawBodyRadius() + P.axleJaw.outerRadius, P.axleJaw.thickness],
      radius: P.axleJaw.outerRadius,
    })
  );

  var body = CAG.roundedRectangle({
    center: [0, 0],
    radius: [P.jawBodyRadius(), P.jawBodyRadius()],
    roundradius: 2,
    resolution: 50,
  }).extrude({offset: [
    0,0,P.axleJaw.profileDepth]
  });

  body = body.union(
    jaw.rotateY(-90).translate([P.jawBodyRadius(), 0, 0])
  );

  body = body.subtract(
    CSG.cube({
      center: [0, 0, -P.axleJaw.outerRadius],
      radius: [P.jawBodyRadius()*2, P.jawBodyRadius()*2, P.axleJaw.outerRadius],
    })
  );

  body = body.subtract(
    chamferedProfile(P.axleJaw.profileDepth)
  );

  return body.subtract(
    CSG.cylinder({
      start: [-P.jawBodyRadius(), 0, P.axleJaw.profileDepth/2],
      end: [0, 0, P.axleJaw.profileDepth/2],
      radius: 2,
    })
  );
}

function dialHolder() {
  var dh = P.dialHolder;

  var body = CAG.roundedRectangle({
    center: [0, 0],
    radius: [P.jawBodyRadius(), P.jawBodyRadius()],
    roundradius: dh.roundRadius,
    resolution: 50,
  }).extrude({offset: [
    0,0,dh.height
  ]});

  body = body.subtract(
    chamferedProfile(dh.height)
  );

  return body.union(
    CAG.roundedRectangle({
      center: [0, P.jawBodyRadius() + dh.offset - dh.roundRadius/2],
      radius: [dh.slot.length/2 + dh.offset/2, dh.offset + dh.roundRadius/2],
      roundradius: 2,
      resolution: 50,
    }).subtract(
        CAG.roundedRectangle({
          center: [0, P.jawBodyRadius() + dh.offset + 1],
          radius: [dh.slot.length/2, dh.slot.radius],
          roundradius: 2,
          resolution: 50,
        })
    ).extrude({offset:[0, 0, dh.slot.thickness]})
  );
}

function bracketFlatBottom(noBottom=false) {
  var h = (P.jawBodyRadius() + 10)*2;

  var body = CAG.roundedRectangle({
    center: [0, 0],
    radius: [P.jawBodyRadius(), P.jawBodyRadius()],
    roundradius: 2,
    resolution: 50,
  });

  if(noBottom) {
    body = body.subtract(
      CAG.rectangle({
        center: [P.TSlot.width/2 + P.axleJaw.wallThickness/2, 0],
        radius: [P.axleJaw.wallThickness/2 + 1, P.jawBodyRadius()],
      })
    ).subtract(
      // get rid of the lone channel
      CAG.fromPoints([
        [0, 0],
        [P.TSlot.width/2, P.TSlot.width/2],
        [P.TSlot.width/2, -P.TSlot.width/2],
      ])
    );
  }

  body = body.extrude({offset: [0, 0, h]});

  body = body.subtract(
    chamferedProfile(h)
  );

  for(var i=-1; i < 2; i += 2) {
    body = body.subtract(
      CSG.cylinder({
        start: [0, 0, h/2 + i*(P.jawBodyRadius() + 5)],
        end: [-P.jawBodyRadius() , 0, h/2 + i*(P.jawBodyRadius() + 5)],
        radius: 2,
      })
    );
  }

  return body;
}

function bracketTop() {
  var h = P.jawBodyRadius()*2;

  var body = CAG.roundedRectangle({
    center: [0, 0],
    radius: [P.jawBodyRadius(), P.jawBodyRadius()],
    roundradius: 2,
    resolution: 50,
  }).union(
    CAG.rectangle({
      center: [P.TSlot.width/2 + P.axleJaw.wallThickness/2, 0],
      radius: [P.axleJaw.wallThickness/2, P.jawBodyRadius() + 10],
    })
  ).extrude({offset: [
    0,0, h
  ]});

  body = body.subtract(
    chamferedProfile(h)
  );

  for(var i=-1; i < 2; i += 2) {
    body = body.subtract(
      CSG.cylinder({
        start: [0, i*(P.jawBodyRadius() + 5), h/2],
        end: [P.jawBodyRadius(), i*(P.jawBodyRadius() + 5), h/2],
        radius: 1.8,
      })
    );
  }

  return body;
}

function main() {
  // return bracketFlatBottom().union(bracketTop().rotateX(90).translate([-P.jawBodyRadius()*2, P.jawBodyRadius(), P.jawBodyRadius()+10]));
  // return bracketTop();
  return bracketFlatBottom();
  return axleJaw();
  return dialHolder();
}