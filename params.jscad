Params = class Params {
	constructor() {
		this.TSlot = {
			width: 20,
		};

		this.axleJaw = {
			profileDepth: 40,
			topHeight: 10,
			innerRadius: 5,
			outerRadius: 15,
			clampThickness: 5,
			thickness: 8,
			wallThickness: 4,
		};

		this.dialHolder = {
			height: 30,
			screwSlotRadius: 3.2,
			offset: 10,
			roundRadius: 2,
			slot: {
				length: 30,
				thickness: 4,
				radius: 3.2,
			},
		};
	}

	jawBodyRadius() {
		return this.TSlot.width/2 + this.axleJaw.wallThickness;
	}

	jawAxleHeight() {
		return this.axleJaw.profileDepth + 0;
	}
};
